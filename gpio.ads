
-- this package is meant to be an easy to use abstraction-layer
-- to the linux interface to the GPIO-Pins of the Raspberry Pi
package gpio is
	-- exports a pin, so it makes it usable
	-- the state of the pin is still undefined! 
	procedure export ( pin : in integer );

	-- unexports the pin. Please unexport the used pins
	-- after usage.
	procedure unexport ( pin : in integer );

	-- sets the pin into the given mode (output = true => outputmode, otherwise
	-- in inputmode. 
	procedure pinMode ( pin: in integer; output: In Boolean );

	-- sets the pin into pull-up mode
	procedure pullUp ( pin : in integer );
	-- sets the pin into pull-down mode
	procedure pullDown ( pin : in integer );
	
	-- will write a value (LOW or HIGH) to the given pin
	procedure digitalWrite ( pin: in integer; value: in Boolean );

	-- will read the value of the given pin
	function digitalRead ( pin: in integer ) return Boolean;
end gpio;

