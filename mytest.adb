with gpio;
with Ada.Text_IO;

use Ada.Text_IO;

procedure mytest is
	pins : array (1..12) of Integer :=
		( 2, 3, 4, 7, 8, 9, 10, 11, 14, 15, 17, 18 ); 

	b : Boolean := True;
begin
	for i in pins'range loop
		gpio.export ( pins(i) );
		gpio.pinMode ( pins(i), True );
	end loop;

	for current in pins'range loop
		for i in pins'range loop 
			gpio.digitalWrite ( pins(i), False );
		end loop;
		gpio.digitalWrite ( pins(current), True );
		delay Duration ( 1.0 );
	end loop;
	for i in pins'range loop
		gpio.unexport ( pins(i) );
	end loop;

	delay duration (0.5);
	
	gpio.pullUp ( pins(12) );
	gpio.export ( pins(12) );
	delay duration(1);

	gpio.pinMode ( pins(12), False );
	delay duration (1);

	while (gpio.digitalRead ( pins(12) ) = True ) loop
		delay DURATION (0.5);
	end loop;

	Ada.Text_IO.Put_line ("Button has been pressed!");

	gpio.unexport ( pins(12) );
exception
	when others =>
		for i in pins'range loop
			gpio.unexport ( pins(i) );
		end loop;
end;
