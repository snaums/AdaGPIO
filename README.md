# AdaGPIO

## Building

Building the GPIO-Package is done by using the package within another program (via the _with_ statement) and calling

    gnatmake mytest.adb

## Usage

See gpio.ads for an interface. There are some functions and procedures not defined in the interface-file, only used within the package. You probably could use them, by putting them into the ads-file, but I see no point in that. 

I tried to be as close to the WiringPi Interface as possible, because that is quite easy to understand.

## Requirements

The gpio-package uses the GPIO-Interface found in the Linux-filesystem in /sys/class/gpio/... . For adding PullUp and PullDown-functionality it uses the command-line-tool _gpio_, which is probably included by eather wiringPi or Raspbian. I tested the code on Raspbian 8 with an Raspberry Pi Model B+. 
